#include <iostream>
#include <string>
#include "Modoestoque.h"
#include <vector>
#include <algorithm>

using namespace std;



void Produto::set_nome(){
	string name;
	cin >> name;
	nome = name;
}

string Produto::get_nome(){
	return nome;
}

vector<string> Produto::get_categorias(){
	return categorias;
}

int Produto::get_categorias_size(){
	return categorias.size();
}

string Produto::get_categoria(int i){
	return categorias[i];
}

void Produto::set_preco(){
	float price;
	cin >> price;
	preco = price;
}

float Produto::get_preco(){
	return preco;
}

void Produto::set_quantidade(){
	int amount;
	cin >> amount;
	quantidade = amount;
}

int Produto::get_quantidade(){
	return quantidade;
}

void Produto::set_quantidadeD(int newq){
	quantidade = newq;
}	

void Produto::add_categoria(){
int aux = 1;
while(aux == 1){
	string newcategory;
	cin >> newcategory;
	categorias.push_back(newcategory);
	cout << "Caso deseje adicionar mais categorias digite 1, caso contrario digite 0" << endl;
	cin >> aux;
}
}

void Produto::print_categoria(){
	for(int i=0; i<categorias.size(); ++i){
  std::cout << categorias[i] << ' ';
}
cout << endl;
}





void Produto::imprime_produto(){
	cout << get_nome () << endl;
	cout << get_preco() << endl;
	cout << get_quantidade() << endl;	
	print_categoria();
}




