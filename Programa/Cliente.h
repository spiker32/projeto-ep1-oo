#ifndef CLIENTE_H
#define CLIENTE_H
#include <string>
#include <vector>

using namespace std;



class Cliente
{
private:
	string nome;
	vector<string> categoriapref;
	vector<int> peso;
public:

	void set_nome();
	string get_nome();

	void add_categoriapref(string newnom,int novop);
	int busca_categoriapref(string nom);
	string get_categoria(int i);
	int get_peso(int i);
	int get_categoria_size();


};



















#endif

