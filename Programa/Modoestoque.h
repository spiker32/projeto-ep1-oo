#ifndef MODOESTOQUE_H
#define MODOESTOQUE_H
#include <string>
#include <vector>

using namespace std;


class Produto
{	
	private:

	string nome;
	float preco;
	int quantidade;
	vector<string> categorias;

	public:
	
	

	void set_nome();
	string get_nome();
	
	void set_preco();
	float get_preco();
	
	void set_quantidade();
	int get_quantidade();

	void set_quantidadeD(int newq);	

	void add_categoria();
	void print_categoria();
	vector<string> get_categorias();
	int get_categorias_size();
	string get_categoria(int i);

	void imprime_produto();
	
	

};




#endif
